import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from './product.service';

import { faFolder } from '@fortawesome/free-solid-svg-icons';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import { faTrash } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  faFolder = faFolder;
  faStar = faStar;
  faTrash = faTrash;
  sum = 10;  
  throttle = 300;  
  scrollDistance = 3;
  productList = null;
  start: number = 0;

  constructor(private productService: ProductService,private router: Router){
  }
  
  ngOnInit(): void {
    this.getLists();
  }

  getLists(){
    this.productService.getLists(this.sum).subscribe((response: any) => {
      this.productList = response.products;
    });
  }

  deleteItem(id: any){
    this.productService.deleteItem(id).subscribe((response:any) => {
      if(response.isDeleted){
        this.productService.showSuccess(response.title+' Has Been Deleted', 'Delete Success', {timeOut: 3000, positionClass: 'toast-top-center', progressBar: true});
      }else{
        this.productService.showError(response.title+' Cant be Deleted', 'Delete Failed', {timeOut: 3000, positionClass: 'toast-top-center', progressBar: true});
      }
    });
  }

  onScrollDown(ev:any) {  
    // add another 10 items  
    this.start = this.sum;  
    this.sum += 10;
    // Limit 20 items
    if(this.sum <= 20){
      this.getLists();
    }
  }
}
