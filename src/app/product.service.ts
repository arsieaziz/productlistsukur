import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private urlProduct = 'https://dummyjson.com/products/';
  constructor(private http: HttpClient,private toastr: ToastrService) { }

  showSuccess(message: string | undefined, title: string | undefined, ToastConfig: {}){
    this.toastr.success(message, title, ToastConfig)
  }

  showError(message: string | undefined, title: string | undefined, ToastConfig: {}){
    this.toastr.error(message, title, ToastConfig)
  }
  
  getLists(limit:Number){
    let addParam;
    if(limit != 0){
      addParam = '?limit='+limit;
    }
    return this.http.get(this.urlProduct+addParam);
  }
  getListsSkip(limit:Number,skip:Number){
    let addParam;
    if(limit != 0){
      addParam = '?limit='+limit+'&skip='+skip;
    }
    return this.http.get(this.urlProduct+addParam);
  }
  deleteItem(id:any){
    return this.http.delete(this.urlProduct+id);
  }
}
